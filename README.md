## Selected features

Since this is an example app for demonstration only, I have decided to implement the relevant bits
I haven't implemented the payment feature as it would be very similar as in feature to the code I've already written so far.

Since this is an exercise I haven't implemented any Unit Tests as I would normally do.
If I had more time to implement the application even further I would moke the DB and procedurally generate differt cases in which
each function is invoked with different output and make sure everything still works correctly.


## Example Usage

Add a new car into the system:
`python3 main.py add-new-car CLIO ECO 1234`

Add a new car into the system:
`python3 main.py search-car ECO INNER`
Returned value: `[('CLIO', 'ECO', '1234')]`

Book car:
`python3 main.py book-car 1234 OUTER`

