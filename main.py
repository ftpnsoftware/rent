import click
from user import User
from car import Car
from db import DB


@click.group()
def cli():
    pass


@cli.command()
@click.argument("name")
@click.argument("surname")
def add_new_user(name, surname):
    """Add new User"""
    click.echo(f"Creating new User {name} {surname}!")
    User.add_new(name, surname)


@cli.command()
def init_db():
    """Initialize Database"""
    click.echo(f"Initializing DB")
    DB.init()


@cli.command()
@click.argument("kind")
@click.argument("place")
def search_car(kind, place):
    """Search for a new car with the given criteria"""
    Car.search_car(kind, place)


@cli.command()
@click.argument("plate")
@click.argument("destination")
def book_car(plate, destination):
    """Book the selected car"""
    Car.book_car(plate, destination)


@cli.command()
@click.argument("name")
@click.argument("kind")
@click.argument("plate")
def add_new_car(name, kind, plate):
    """Register a new car into the system"""
    click.echo(f"Creating new Car {name} {kind} {plate}!")
    Car.add_new(name, kind, plate)


if __name__ == "__main__":
    cli()
