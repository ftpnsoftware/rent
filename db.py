import sqlite3


class DB:
    @staticmethod
    def init():
        con = sqlite3.connect("rent.db")
        cur = con.cursor()
        cur.execute(
            f"""
            CREATE TABLE User(name,surname);
            """
        )
        con.commit()
        cur.execute(
            f"""
            CREATE TABLE Car(name,kind,plate,place,distance_traveled,availability);
            """
        )
        con.commit()
