import sqlite3
from enum import Enum


class CarKind(Enum):
    ECO = 1
    MID = 2
    DELUXE = 3


class Places(Enum):
    INNER = 1
    MID = 2
    OUTER = 3


class Car:
    def __init__(self, name, kind, plate):
        self.name = name
        self.kind = kind

    @staticmethod
    def add_new(name, kind, plate):
        """Add a new Car into the system"""
        con = sqlite3.connect("rent.db")
        cur = con.cursor()
        cur.execute(
            f"""
        INSERT INTO Car VALUES
            ('{name}', '{kind}', '{plate}', 'INNER', 0, 'AVAILABLE')
        """
        )
        con.commit()

    @staticmethod
    def search_car(kind, place):
        """Look for a car with the given criteria"""
        con = sqlite3.connect("rent.db")
        cur = con.cursor()
        result = cur.execute(
            f"""
            SELECT name, kind, plate from Car 
            where kind == '{kind}' and place == '{place}' and availability == 'AVAILABLE';
        """
        )
        print(result.fetchall())

    @staticmethod
    def book_car(plate, destination):
        """Book Car for the User"""
        new_distance = Car.compute_traveled_distance(
            plate, Car.get_current_position(plate), destination
        )
        Car.update_car(plate, destination, new_distance)

    @staticmethod
    def update_car(plate, destination, new_distance):
        """Mark the car as unavailable"""
        con = sqlite3.connect("rent.db")
        cur = con.cursor()
        result = cur.execute(
            f"""
            Update Car set availability = 'UNAVAILABLE', place= '{destination}' where plate == '{plate}';
        """
        )
        print(result.fetchall())
        con.close()

    @staticmethod
    def get_current_position(plate):
        """Get the current car position"""
        con = sqlite3.connect("rent.db")
        cur = con.cursor()
        result = cur.execute(
            f"""
            SELECT place from Car 
            where plate == '{plate}';
        """
        )
        position = result.fetchone()
        con.close()
        return position[0]

    @staticmethod
    def compute_traveled_distance(plate, origin, destination):
        """Compute the distance traveled for the given booking"""
        con = sqlite3.connect("rent.db")
        cur = con.cursor()
        result = cur.execute(
            f"""
            SELECT distance_traveled from Car 
            where plate == '{plate}';
        """
        )
        distance_traveled = result.fetchone()[0]
        if origin == destination:
            distance_traveled += 1
        else:
            distance_traveled = 1 + abs(
                Places[destination].value - Places[origin].value
            )
        con.close()
        return distance_traveled
