import sqlite3


class User:
    def __init__(self, name, surname):
        self.name = name
        self.surname = surname

    @staticmethod
    def add_new(name, surname):
        """Add a new User into the system"""
        con = sqlite3.connect("rent.db")
        cur = con.cursor()
        cur.execute(
            f"""
        INSERT INTO User VALUES
            ('{name}', '{surname}')
        """
        )
        con.commit()
